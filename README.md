# Formation Master 2 Bioinformatique de l'Université Clermont Auvergne - Unité d'Enseignement "Calculs parallèles et initiation à la programmation GPU"



## Description

Here scripts example and data set metadata from "STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse"
David Gomez-Cabrero et al. 2019 https://doi.org/10.1038/s41597-019-0202-7. Objective is to reproduce miRNA-seq analysis on a HPC infrastructure using SLURM as scheduler.

## Directory tree structure

├── README.md  
├── scripts  
│   ├── miRNA_wf.sh  
│   ├── mirna_mapping.slurm  
│   ├── mirna_qc-init.slurm  
│   ├── mirna_qc-post.slurm  
│   └── mirna_trim.slurm  
├─── data  
│   ├── reference.loc  
│   ├── mmu-GRCm39-mature-miRNA_refseq-names.gff3  
│   ├── des.mat    
│   ├── data.loc   
│   └── subset.loc  
├─── log  


## Requirements

This scripts can run on a HPC infrastructure using Lmod to manage environment. If tools are not available in Lmod, conda module can be used to make tools accessible.

## Usage

To collect scripts and datasets, use the git command line client:

git clone https://gitlab.com/clsu/uca_m2bi_hpc_mirna_SU.git

This will create a uca_m2bi_hpc_mirna_SU repository in your directory.



Launching a job is looking like:

```
[student05@ws2023-master ~]$ cd uca_m2bi_hpc_mirna_SU/
[student05@ws2023-master uca_m2bi_hpc_mirna_SU]$ sbatch scripts/qc-post.slurm
```

From uca_m2bi_hpc_mirna_SU depository, before launching a job :

```
[student05@ws2023-master uca_m2bi_hpc_mirna_SU]$ bash scripts/conda_module_load.sh
```
Execute manually the following line : conda init bash .
```
[student05@ws2023-master uca_m2bi_hpc_mirna_SU]$ conda init bash
```
After execute the line close and reopen your terminal to apply the changes and launch the miRNA workflow:

From uca_m2bi_hpc_mirna_SU depository, launch the miRNA workflow like that:

```
[student05@ws2023-master uca_m2bi_hpc_mirna_SU]$ bash scripts/miRNA_wf.sh
```

## Licence

Creative Commons Legal Code - CC0 1.0 Universal

## Authors and acknowledgment
Thanks to Nadia GOUE

