#! /bin/bash
#cd "$(dirname "$0")"

echo 'Date: Fall Master course 2023'
echo 'Object: Sample workflow for Stategra miRNA-seq datasets showing job execution and dependency handling.'
echo 'Inputs: paths to scripts for quality control, triming, mapping, quantification, filtring and normalization'
echo 'Outputs: trimmed fastq files, QC HTML files, mapped fastq files, count files and normalized files'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose

IFS=$'\n\t'


# first job - no dependencies
# Initial QC
jid1=$(sbatch --parsable scripts/mirna_qc-init.slurm)

echo "$jid1 : Initial Quality Control"

# Trimming
jid2=$(sbatch --parsable --dependency=afterok:$jid1 scripts/mirna_trim.slurm)

echo "$jid2 : Trimming with Trimmomatic tool"

# Post QC
jid3=$(sbatch --parsable --dependency=afterok:$jid2 scripts/mirna_qc-post.slurm)

echo "$jid3 : Post control_quality"

# Mapping
jid4=$(sbatch --parsable --dependency=afterok:$jid3 scripts/mirna_mapping.slurm)

echo "$jid4 : Mapping with BWA tool"

# first job - no dependencies
# Sorting alignment files
jid5=$(sbatch --parsable --dependency=afterok:$jid4 scripts/mirna_sort_align.slurm)

echo "$jid5 : Sorting alignment files with samtools"

# Quantification
jid6=$(sbatch --parsable --dependency=afterok:$jid5 scripts/mirna_quantification.slurm)

echo "$jid6 : Quantification with Bedtools multiBamCov tool"

# Normalization
jid7=$(sbatch --parsable --dependency=afterok:$jid6 scripts/mirna_normalization.slurm)

echo "$jid7 : Normalization with R: NOISeq and sva library "

