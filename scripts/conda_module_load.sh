#! /bin/bash

# Upload whatever required packages
module purge
module load conda/4.12.0

#install mumba
conda install mamba -n base -c conda-forge -y
conda update conda -y
conda update --all

#install r
mamba create -n R -c conda-forge r-base -y
conda activate R

#install r package 
mamba install r-biocmanager
mamba install -c bioconda bioconductor-noiseq
mamba install -c bioconda bioconductor-sva


# Create Conda environment with Python 3.8
conda create --name mirna-seq python=3.8

# Activate Conda environment
conda activate mirna-seq

# Install required packages
conda install -c bioconda samtools=1.9 # latest version, samtools had an issue with not finding a shared library libcrypto.so.1.0.0
conda install -c bioconda bedtools  # for multiBamCov


conda deactivate
conda deactivate


